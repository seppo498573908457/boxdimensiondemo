﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BoxDimensionDemo
{
    public partial class BoxDimensionDemo : Form
    {

        private class DimensionResult
        {
            public bool Go;
            public double Value;
            public TextBox Txt;
        }

        public BoxDimensionDemo()
        {
            InitializeComponent();
            this.checkFlash.Tick += CheckFlash_Tick;
        }

        private void BoxDimensionDemo_Load(object sender, EventArgs e)
        {
            txtVolume_TextChanged(this.txtVolume, null);
        }

        private void txtVolume_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                ResetByVolume();
            }
        }

        private void txtVolume_TextChanged(object sender, EventArgs e)
        {
            ResetByVolume();
        }

        private void ResetByVolume()
        {
            if (ParseInput(this.txtThickness, out var t))
            {
                t *= 2;
            }
            else
            {
                return;
            }

            if (ParseInput(this.txtVolume, out var dm3))
            {
                // Reset all to cube.
                var mm = Math.Round(Math.Pow(dm3 * 1000000.0, 1.0 / 3.0), MidpointRounding.AwayFromZero);
                mm += t;
                this.txtDepth.Text = mm.ToString();
                this.txtHeight.Text = mm.ToString();
                this.txtWidth.Text = mm.ToString();
            }
        }

        private void txtThickness_TextChanged(object sender, EventArgs e)
        {
            txtVolume_TextChanged(this.txtVolume, null);
        }

        private List<DimensionResult> GetDimensions(TextBox trigger, double thickness)
        {
            var list = new List<DimensionResult>();

            if (ParseInput(this.txtDepth, out var d, thickness))
            {
                list.Add(new DimensionResult
                {
                    Go = !ReferenceEquals(trigger, this.txtDepth) && !this.chbLockDepth.Checked,
                    Value = d - thickness,
                    Txt = this.txtDepth
                });
            }
            else
            {
                return null;
            }

            if (ParseInput(this.txtHeight, out var h, thickness))
            {
                list.Add(new DimensionResult
                {
                    Go = !ReferenceEquals(trigger, this.txtHeight) && !this.chbLockHeight.Checked,
                    Value = h - thickness,
                    Txt = this.txtHeight
                });
            }
            else
            {
                return null;
            }

            if (ParseInput(this.txtWidth, out var w, thickness))
            {
                list.Add(new DimensionResult
                {
                    Go = !ReferenceEquals(trigger, this.txtWidth) && !this.chbLockWidth.Checked,
                    Value = w - thickness,
                    Txt = this.txtWidth
                });
            }
            else
            {
                return null;
            }

            return list;
        }

        private static bool ParseInput(TextBox txt, out double value, double? greaterThan = null)
        {
            if (double.TryParse(txt.Text, out value)
                && (!greaterThan.HasValue || value > greaterThan.Value)
                )
            {
                return true;
            }
            else
            {
                ErronousInput(txt);
                return false;
            }
        }

        private static void ErronousInput(TextBox txt)
        {
            // Flash input
            txt.BackColor = Color.PaleVioletRed;
            var timer = new Timer() { Interval = 1000, Tag = txt };
            timer.Tick += FlashTimer_Tick;
            timer.Start();
        }

        private static void FlashTimer_Tick(object sender, EventArgs e)
        {
            // Just one flash
            var timer = (Timer)sender;
            var txt = (TextBox)timer.Tag;
            txt.BackColor = Color.White;
            timer.Stop();
            timer.Dispose();
        }

        private void txtDimension_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r')
            {
                var trigger = (TextBox)sender;
                DoCalc(trigger);
            }
        }

        private void txtDimension_Leave(object sender, EventArgs e)
        {
            var trigger = (TextBox)sender;
            DoCalc(trigger);
        }

        private void DoCalc(TextBox trigger)
        {
            if (ParseInput(this.txtThickness, out var t))
            {
                t *= 2;
            }
            else
            {
                FailCheck();
                return;
            }

            var list = GetDimensions(trigger, t);

            if (list == null)
            {
                FailCheck();
                return;
            }

            if (ParseInput(this.txtVolume, out var mm3))
            {
                mm3 *= 1000000.0; // dm3 to mm3
            }
            else
            {
                FailCheck();
                return;
            }

            foreach (var item in list.ToArray())
            {
                if (!item.Go)
                {
                    mm3 /= item.Value;
                    list.Remove(item);
                }
            }

            if (list.Count == 0)
            {
                // No inputs to modify
                ErronousInput(trigger);
                FailCheck();
                return;
            }
            else if (list.Count > 1)
            {
                mm3 = Math.Pow(mm3, 1.0 / (double)list.Count);
            }

            foreach (var item in list)
            {
                // Re-apply thickness.
                item.Txt.Text = Math.Round(mm3 + t, MidpointRounding.AwayFromZero).ToString();
            }

            // Check
            DoCheck();
        }

        #region Check

        private Timer checkFlash = new Timer() { Interval = 1000 };

        private void FailCheck()
        {
            this.checkFlash.Stop();
            this.lblCheck.Text = "...";
            this.lblCheck.BackColor = Color.PaleVioletRed;
            this.checkFlash.Start();
        }

        private void DoCheck()
        {
            this.checkFlash.Stop();

            double.TryParse(this.txtVolume.Text, out var v);
            double.TryParse(this.txtThickness.Text, out var t);
            double.TryParse(this.txtDepth.Text, out var d);
            double.TryParse(this.txtHeight.Text, out var h);
            double.TryParse(this.txtWidth.Text, out var w);

            t *= 2;
            var dm3 = (d - t) * (h - t) * (w - t) / 1000000.0;
            this.lblCheck.Text = Math.Round(dm3, 3, MidpointRounding.AwayFromZero).ToString();

            if (Math.Round(v, MidpointRounding.AwayFromZero) == Math.Round(dm3, MidpointRounding.AwayFromZero))
            {
                this.lblCheck.BackColor = Color.LightYellow;
            }
            else
            {
                this.lblCheck.BackColor = Color.GreenYellow;
            }

            this.checkFlash.Start();
        }

        private void CheckFlash_Tick(object sender, EventArgs e)
        {
            this.lblCheck.BackColor = Color.Transparent;
            this.checkFlash.Stop();
        }

        #endregion

    }
}
