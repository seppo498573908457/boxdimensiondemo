﻿namespace BoxDimensionDemo
{
    partial class BoxDimensionDemo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtDepth = new System.Windows.Forms.TextBox();
            this.txtHeight = new System.Windows.Forms.TextBox();
            this.txtWidth = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.chbLockWidth = new System.Windows.Forms.CheckBox();
            this.chbLockHeight = new System.Windows.Forms.CheckBox();
            this.chbLockDepth = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtThickness = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtVolume = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblCheck = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtDepth
            // 
            this.txtDepth.Location = new System.Drawing.Point(102, 140);
            this.txtDepth.Name = "txtDepth";
            this.txtDepth.Size = new System.Drawing.Size(100, 26);
            this.txtDepth.TabIndex = 6;
            this.txtDepth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDimension_KeyPress);
            this.txtDepth.Leave += new System.EventHandler(this.txtDimension_Leave);
            // 
            // txtHeight
            // 
            this.txtHeight.Location = new System.Drawing.Point(102, 108);
            this.txtHeight.Name = "txtHeight";
            this.txtHeight.Size = new System.Drawing.Size(100, 26);
            this.txtHeight.TabIndex = 4;
            this.txtHeight.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDimension_KeyPress);
            this.txtHeight.Leave += new System.EventHandler(this.txtDimension_Leave);
            // 
            // txtWidth
            // 
            this.txtWidth.Location = new System.Drawing.Point(102, 76);
            this.txtWidth.Name = "txtWidth";
            this.txtWidth.Size = new System.Drawing.Size(100, 26);
            this.txtWidth.TabIndex = 2;
            this.txtWidth.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDimension_KeyPress);
            this.txtWidth.Leave += new System.EventHandler(this.txtDimension_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 79);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Width:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Depth:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 5;
            this.label3.Text = "Height:";
            // 
            // chbLockWidth
            // 
            this.chbLockWidth.AutoSize = true;
            this.chbLockWidth.Location = new System.Drawing.Point(249, 78);
            this.chbLockWidth.Name = "chbLockWidth";
            this.chbLockWidth.Size = new System.Drawing.Size(87, 24);
            this.chbLockWidth.TabIndex = 3;
            this.chbLockWidth.Text = "Locked";
            this.chbLockWidth.UseVisualStyleBackColor = true;
            // 
            // chbLockHeight
            // 
            this.chbLockHeight.AutoSize = true;
            this.chbLockHeight.Location = new System.Drawing.Point(249, 111);
            this.chbLockHeight.Name = "chbLockHeight";
            this.chbLockHeight.Size = new System.Drawing.Size(87, 24);
            this.chbLockHeight.TabIndex = 5;
            this.chbLockHeight.Text = "Locked";
            this.chbLockHeight.UseVisualStyleBackColor = true;
            // 
            // chbLockDepth
            // 
            this.chbLockDepth.AutoSize = true;
            this.chbLockDepth.Location = new System.Drawing.Point(249, 142);
            this.chbLockDepth.Name = "chbLockDepth";
            this.chbLockDepth.Size = new System.Drawing.Size(87, 24);
            this.chbLockDepth.TabIndex = 7;
            this.chbLockDepth.Text = "Locked";
            this.chbLockDepth.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "mm";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(208, 111);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 20);
            this.label5.TabIndex = 10;
            this.label5.Text = "mm";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(208, 143);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 20);
            this.label6.TabIndex = 11;
            this.label6.Text = "mm";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 47);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 20);
            this.label7.TabIndex = 12;
            this.label7.Text = "Thickness:";
            // 
            // txtThickness
            // 
            this.txtThickness.Location = new System.Drawing.Point(102, 44);
            this.txtThickness.Name = "txtThickness";
            this.txtThickness.Size = new System.Drawing.Size(100, 26);
            this.txtThickness.TabIndex = 1;
            this.txtThickness.Text = "0";
            this.txtThickness.TextChanged += new System.EventHandler(this.txtThickness_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(208, 47);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 20);
            this.label8.TabIndex = 14;
            this.label8.Text = "mm";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(208, 15);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 20);
            this.label9.TabIndex = 17;
            this.label9.Text = "l";
            // 
            // txtVolume
            // 
            this.txtVolume.Location = new System.Drawing.Point(102, 12);
            this.txtVolume.Name = "txtVolume";
            this.txtVolume.Size = new System.Drawing.Size(100, 26);
            this.txtVolume.TabIndex = 0;
            this.txtVolume.Text = "10";
            this.txtVolume.TextChanged += new System.EventHandler(this.txtVolume_TextChanged);
            this.txtVolume.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVolume_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(12, 15);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 20);
            this.label10.TabIndex = 15;
            this.label10.Text = "Volume:";
            // 
            // lblCheck
            // 
            this.lblCheck.AutoSize = true;
            this.lblCheck.Location = new System.Drawing.Point(98, 181);
            this.lblCheck.Name = "lblCheck";
            this.lblCheck.Size = new System.Drawing.Size(21, 20);
            this.lblCheck.TabIndex = 18;
            this.lblCheck.Text = "...";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(12, 181);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(58, 20);
            this.label12.TabIndex = 19;
            this.label12.Text = "Check:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(208, 181);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 20);
            this.label13.TabIndex = 20;
            this.label13.Text = "l";
            // 
            // BoxDimensionDemo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(397, 282);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.lblCheck);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtVolume);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtThickness);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.chbLockDepth);
            this.Controls.Add(this.chbLockHeight);
            this.Controls.Add(this.chbLockWidth);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtWidth);
            this.Controls.Add(this.txtHeight);
            this.Controls.Add(this.txtDepth);
            this.Name = "BoxDimensionDemo";
            this.Text = "BoxDimensionDemo";
            this.Load += new System.EventHandler(this.BoxDimensionDemo_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtDepth;
        private System.Windows.Forms.TextBox txtHeight;
        private System.Windows.Forms.TextBox txtWidth;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.CheckBox chbLockWidth;
        private System.Windows.Forms.CheckBox chbLockHeight;
        private System.Windows.Forms.CheckBox chbLockDepth;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtThickness;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtVolume;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblCheck;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
    }
}

